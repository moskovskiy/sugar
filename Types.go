package sugar

// Any types resembles any type, hence any type complies with empty interface
// use Any to send any type ¯\_(ツ)_/¯
type Any = interface{}

// Tuple is an array of any tupes a.k.a array of multiple types
// e.g. [string, bool, int, YourStruct, YourInterface]
type Tuple = []Any

// Dictionary is an array of any types, except where elements are labled with string name (it's a map)
// e.g. ["name":"Alex", "married":false, "age":3, "parent":Parent]
type Dictionary = map[string]Any

//JSONDictionary is just for ease of reading
type JSONDictionary = string
